
function init()
{
	$('input.date').datepicker({ firstDay: 1, dateFormat: 'yy-mm-dd' });
	
	$('input, textarea').focusin( function(event)
	{
		$(this).parent().siblings( '.hint-message-back' ).addClass('hidden');
	} );
	
	$('input, textarea').focusout( function(event)
	{
		if ( $(this).val().length == 0 )
			$(this).parent().siblings( '.hint-message-back' ).removeClass('hidden');
	} );
	
	$('.hint-message-back').click( function(event)
	{
		$('input, textarea', $(this).parent() ).focus();
	} );
}

$(init);
