<?php

class m000000_000021_events extends CDbMigration
{
	public function safeUp()
	{
		$this->execute(
<<<'EOD'
			CREATE TABLE `events` (
				`id` int unsigned NOT NULL AUTO_INCREMENT,
				`session_id` int unsigned NOT NULL,
				`timestamp` bigint unsigned NOT NULL,
				`thread_id` int unsigned NOT NULL,
				`owner_user_id` int unsigned NOT NULL,
				`old_event_id` int unsigned DEFAULT NULL,
				`uri_name` char(50) DEFAULT NULL,
				`event_type` varchar(50) NOT NULL,
				`title` varchar(100) NOT NULL,
				`summary` varchar(255) DEFAULT NULL,
				`datetime_begin` datetime NOT NULL,
				`datetime_end` datetime DEFAULT NULL,
				`gps_track` varchar(255) DEFAULT NULL,
				`start_location` varchar(255) DEFAULT NULL,
				`picture_url` varchar(255) DEFAULT NULL,
				
				PRIMARY KEY (`id`),
				KEY `session_id` (`session_id`),
				KEY `timestamp` (`timestamp`),
				KEY `thread_id` (`thread_id`),
				KEY `owner_user_id` (`owner_user_id`),
				KEY `old_event_id` (`old_event_id`),
				UNIQUE KEY `uri_name` (`uri_name`),
				KEY `event_type` (`event_type`),
				KEY `datetime_begin` (`datetime_begin`),
				
				CONSTRAINT `events_ibfk_1`
					FOREIGN KEY (`session_id`)
					REFERENCES `sessions` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE,
				CONSTRAINT `events_ibfk_2`
					FOREIGN KEY (`thread_id`)
					REFERENCES `forum_threads` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE,
				CONSTRAINT `events_ibfk_3`
					FOREIGN KEY (`owner_user_id`)
					REFERENCES `users` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE,
				CONSTRAINT `events_ibfk_4`
					FOREIGN KEY (`old_event_id`)
					REFERENCES `events` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE
			);
EOD
		);
		
		return true;
	}

	public function safeDown()
	{
		$this->dropTable('events');
		return true;
	}
}