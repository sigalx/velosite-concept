<?php

class m000000_000011_forum extends CDbMigration
{
	public function safeUp()
	{
		$this->execute(
<<<'EOD'
			CREATE TABLE `forums` (
				`id` int unsigned NOT NULL AUTO_INCREMENT,
				`name` char(50) NOT NULL,
				`description` varchar(255) DEFAULT NULL,
				`parent_forum_id` int unsigned DEFAULT NULL,
				
				PRIMARY KEY (`id`),
				UNIQUE KEY `name` (`name`),
				KEY `parent_forum_id` (`parent_forum_id`),
				
				CONSTRAINT `forums_ibfk_1`
					FOREIGN KEY (`parent_forum_id`)
					REFERENCES `forums` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE
				
			) AUTO_INCREMENT = 100;
			
			CREATE TABLE `forum_threads` (
				`id` int unsigned NOT NULL AUTO_INCREMENT,
				`session_id` int unsigned NOT NULL,
				`timestamp` bigint unsigned NOT NULL,
				`forum_id` int unsigned NOT NULL,
				`owner_user_id` int unsigned NOT NULL,
				`parent_thread_id` int unsigned DEFAULT NULL,
				`title` varchar(100) NOT NULL,
				`summary` varchar(255) DEFAULT NULL,
				
				PRIMARY KEY (`id`),
				KEY `session_id` (`session_id`),
				KEY `timestamp` (`timestamp`),
				KEY `forum_id` (`forum_id`),
				KEY `owner_user_id` (`owner_user_id`),
				KEY `parent_thread_id` (`parent_thread_id`),
				
				CONSTRAINT `forum_threads_ibfk_1`
					FOREIGN KEY (`session_id`)
					REFERENCES `sessions` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE,
				CONSTRAINT `forum_threads_ibfk_2`
					FOREIGN KEY (`forum_id`)
					REFERENCES `forums` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE,
				CONSTRAINT `forum_threads_ibfk_3`
					FOREIGN KEY (`owner_user_id`)
					REFERENCES `users` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE,
				CONSTRAINT `forum_threads_ibfk_4`
					FOREIGN KEY (`parent_thread_id`)
					REFERENCES `forum_threads` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE
			);
			
			CREATE TABLE `forum_posts` (
				`id` int unsigned NOT NULL AUTO_INCREMENT,
				`session_id` int unsigned NOT NULL,
				`timestamp` bigint unsigned NOT NULL,
				`thread_id` int unsigned NOT NULL,
				`owner_user_id` int unsigned NOT NULL,
				`parent_post_id` int unsigned DEFAULT NULL,
				`old_post_id` int unsigned DEFAULT NULL,
				`message` TEXT NOT NULL,
				
				PRIMARY KEY (`id`),
				KEY `session_id` (`session_id`),
				KEY `timestamp` (`timestamp`),
				KEY `thread_id` (`thread_id`),
				KEY `owner_user_id` (`owner_user_id`),
				KEY `parent_post_id` (`parent_post_id`),
				UNIQUE KEY `old_post_id` (`old_post_id`),
				
				CONSTRAINT `forum_posts_ibfk_1`
					FOREIGN KEY (`session_id`)
					REFERENCES `sessions` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE,
				CONSTRAINT `forum_posts_ibfk_2`
					FOREIGN KEY (`thread_id`)
					REFERENCES `forum_threads` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE,
				CONSTRAINT `forum_posts_ibfk_3`
					FOREIGN KEY (`owner_user_id`)
					REFERENCES `users` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE,
				CONSTRAINT `forum_posts_ibfk_4`
					FOREIGN KEY (`parent_post_id`)
					REFERENCES `forum_posts` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE,
				CONSTRAINT `forum_posts_ibfk_5`
					FOREIGN KEY (`old_post_id`)
					REFERENCES `forum_posts` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE
			);
			
			INSERT INTO `forums` (`id`, `name`) VALUES
				(1, 'system'),
				(10, 'events'),
				(11, 'articles'),
				(12, 'forum'),
				(13, 'market');
EOD
		);
		
		return true;
	}

	public function safeDown()
	{
		$this->dropTable('forum_posts');
		$this->dropTable('forum_threads');
		$this->dropTable('forums');
		return true;
	}
}