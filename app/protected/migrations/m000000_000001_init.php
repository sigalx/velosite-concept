<?php

class m000000_000001_init extends CDbMigration
{
	public function safeUp()
	{
		$this->execute(
<<<'EOD'
			CREATE TABLE `roles` (
				`id` int unsigned NOT NULL AUTO_INCREMENT,
				`name` char(50) NOT NULL,
				`description` varchar(255) DEFAULT NULL,
				
				PRIMARY KEY (`id`),
				UNIQUE KEY `name` (`name`)
			) AUTO_INCREMENT = 100;
			
			CREATE TABLE `users` (
				`id` int unsigned NOT NULL AUTO_INCREMENT,
				`role_id` int unsigned NOT NULL,
				`login` char(50) NOT NULL,
				`hash` char(128) NOT NULL,
				`salt` char(128) NOT NULL,
				`register_time` bigint unsigned NOT NULL,
				`cannot_change_password` boolean DEFAULT NULL,
				`password_expiration` bigint unsigned DEFAULT NULL,
				`disabled` boolean DEFAULT NULL,
				`locked_till` bigint unsigned DEFAULT NULL,
				`name` varchar(255) DEFAULT NULL,
				`email` varchar(255) DEFAULT NULL,
				`vkontakte_id` bigint unsigned DEFAULT NULL,
				
				PRIMARY KEY (`id`),
				KEY `role_id` (`role_id`),
				KEY `register_time` (`register_time`),
				UNIQUE KEY `login` (`login`),
				UNIQUE KEY `email` (`email`),
				UNIQUE KEY `vkontakte_id` (`vkontakte_id`),
				
				CONSTRAINT `users_ibfk_1`
					FOREIGN KEY (`role_id`)
					REFERENCES `roles` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE
			);
			
			CREATE TABLE `sessions` (
				`id` int unsigned NOT NULL AUTO_INCREMENT,
				`user_id` int unsigned NOT NULL,
				`hash` char(128) NOT NULL,
				`start_time` bigint unsigned NOT NULL,
				`close_time` bigint unsigned DEFAULT NULL,
				`ipaddr` int unsigned NOT NULL,
				
				PRIMARY KEY (`id`),
				UNIQUE KEY `hash` (`hash`),
				
				KEY `user_id` (`user_id`),
				KEY `start_time` (`start_time`),
				KEY `close_time` (`close_time`),
				KEY `ipaddr` (`ipaddr`),
				CONSTRAINT `sessions_ibfk_1`
					FOREIGN KEY (`user_id`)
					REFERENCES `users` (`id`)
					ON DELETE CASCADE ON UPDATE CASCADE
			);
			
			INSERT INTO `roles` (`id`, `name`) VALUES
				(1, 'root'),
				(2, 'administrator'),
				(10, 'authenticated'),
				(50, 'anonymous');
EOD
		);
		
		$this->insert('users', array(
			'role_id' => 1,
			'login' => 'root',
			'hash' => 'root', // not able to login
			'salt' => 'root',
			'register_time' => 0,
		));
		
		$this->insert('users', array(
			'role_id' => 50,
			'login' => 'anonymous',
			'hash' => 'anonymous', // not able to login
			'salt' => 'anonymous',
			'register_time' => 0,
		));
		
		$this->insert('users', array(
			'role_id' => 2,
			'login' => 'admin',
			'hash' => '03Yw9Hmgr/J2FE9x7Ghz2oiYqkfGHqGeSNBy2YkOEksBpWWHTnz15/9TumgK51L8OYwh32oOXvjQEx0BLzCJk0', // admin
			'salt' => 'd453lU9uWdNqIht0oUIcbvqsEbKVxd7X6h2S4ibpr7F6mUKz4tdZntVofU9EmD4DB5ala3A45fz19t7UBlVKoh1rouE5N0m1r76dd2ohk6hbRP86LtGc8VVqPUAo2h86',
			'register_time' => 0,
		));
		
		return true;
	}

	public function safeDown()
	{
		$this->dropTable('sessions');
		$this->dropTable('users');
		$this->dropTable('roles');
		return true;
	}
}