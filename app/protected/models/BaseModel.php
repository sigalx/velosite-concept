<?php

class BaseModel extends CActiveRecord
{
	public static function generateHash($message, $salt = '')
	{
		if ( !CRYPT_SHA512 )
			trigger_error('CRYPT_SHA512 is not defined');
		
		return end(explode('$', crypt(Yii::app()->params['salt'].$message.$salt,
			'$6$rounds=5000$'.$salt.Yii::app()->params['salt'].'$')));
	}
	
	public static function ipaddrToInt($ipaddr)
	{
		$matches = null;
		if ( !preg_match('/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/', $ipaddr, $matches) )
			return false;
		
		if (count($matches) != 5)
			return false;
		
		$result = 0;
		
		for ( $n = 1; $n < 5; $n++ )
			$result += intval($matches[$n]) * pow(256, 4 - $n);
		
		return $result;
	}
};
