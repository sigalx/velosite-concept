<?php

class Sessions extends BaseModel
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return 'sessions';
	}

	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'start_time' => 'Start Time',
			'close_time' => 'Close Time',
			'ipaddr' => 'Ipaddr',
		);
	}
	
	public function findByAuthHash($auth_hash)
	{
		$session = $this->with('user')->findByAttributes( array(
			'hash' => static::generateHash( $auth_hash, $_SERVER['REMOTE_ADDR'] )
		));
		
		if ( $session === null ||
		     $session->close_time && $session->close_time < time() )
			return false;
		
		return $session;
	}
	
	public static function create($user_id, $auth_hash, $close_time = null)
	{
		$session = new Sessions;
		
		$session->user_id = $user_id;
		$session->hash = static::generateHash( $auth_hash, $_SERVER['REMOTE_ADDR'] );
		$session->start_time = time();
		$session->close_time = $close_time;
		$session->ipaddr = static::ipaddrToInt($_SERVER['REMOTE_ADDR']);
		
		if ( !$session->save() )
			return false;
		
		return $session;
	}
}