<?php

class ForumThreads extends CActiveRecord
{
	const system_forum_id = 1;
	const events_forum_id = 10;
	const articles_forum_id = 11;
	const threads_forum_id = 12;
	const market_forum_id = 13;
	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return 'forum_threads';
	}

	public function rules()
	{
		return array(
			array('title', 'required', 'message' => 'Поле «{attribute}» должно быть заполнено'),
			array('title', 'length', 'max' => 100),
			array('summary', 'length', 'max' => 255),
			array('title, summary', 'safe'),
		);
	}
	
	public function relations()
	{
		return array(
		
			'session' => array(self::BELONGS_TO, 'Sessions', 'session_id'),
			'forum' => array(self::BELONGS_TO, 'Forums', 'forum_id'),
			'owner_user' => array(self::BELONGS_TO, 'Users', 'owner_user_id'),
			'parent_thread' => array(self::BELONGS_TO, 'ForumThreads', 'parent_thread_id'),
			
			'events' => array(self::HAS_MANY, 'Events', 'thread_id'),
			'posts' => array(self::HAS_MANY, 'ForumPosts', 'thread_id'),
			'children_threads' => array(self::HAS_MANY, 'ForumThreads', 'parent_thread_id'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'owner_user_id' => 'Автор',
			'title' => 'Заголовок',
			'summary' => 'Краткое описание',
		);
	}
	
	public function searchForumThreads()
	{
		$criteria = new CDbCriteria;
		
		$criteria->compare('forum_id', ForumThreads::threads_forum_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	
	public function searchPostsHierarchical()
	{
		return ForumPosts::model()->searchInThreadHierarchical($this->id);
	}
	
	public function create($data)
	{
		$this->setAttributes($data, false);
		
		$this->session_id = Yii::app()->user->session->id;
		$this->timestamp = time();
		
		if ( empty($this->owner_user_id) )
			$this->owner_user_id = Yii::app()->user->id;
		
		return $this->save();
	}
}
