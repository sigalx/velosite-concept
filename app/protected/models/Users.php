<?php

class Users extends BaseModel
{
	const root_role_id = 1;
	const administrator_role_id = 2;
	const authenticated_role_id = 10;
	const anonymous_role_id = 50;
	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return 'users';
	}
	
	public function relations()
	{
		return array(
			'role' => array(self::BELONGS_TO, 'Roles', 'role_id'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => 'Login',
			'name' => 'Name',
		);
	}
	
	public function findByLogin($login)
	{
		return $this->findByAttributes( array('login' => $login) );
	}
	
	public function findByVkontakteId($vkontakte_id)
	{
		return $this->findByAttributes( array('vkontakte_id' => $vkontakte_id) );
	}
	
	public function authenticateByPassword($login, $password)
	{
		$user = $this->findByLogin($login);
		
		if ($user === null)
			return false;
		
		if ( $user->hash !== static::generateHash( $login.$password, $user->salt ) )
			return false;
		
		return $user;
	}
	
	public function create($role_id, $login, $password = null, $cannot_change_password = null, $password_expiration = null)
	{
		$user = new Users;
		
		$user->role_id = $role_id;
		$user->login = $login;
		$user->salt = base64_encode( BaseModel::generateHash( $role_id.$login.uniqid(), time() ) );
		$user->hash = $password ? static::generateHash( $login.$password, $user->salt ) : $login;
		$user->register_time = time();
		$user->cannot_change_password = $cannot_change_password;
		$user->password_expiration = $password_expiration;
		
		if ( !$user->save() )
			return false;
		
		return $user;
	}
}