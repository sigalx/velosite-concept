<?php

class ForumPosts extends BaseModel
{
	public $reply_posts = array();
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return 'forum_posts';
	}

	public function rules()
	{
		return array(
			array('message', 'required', 'message' => 'Поле «{attribute}» должно быть заполнено'),
			array('message', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'session' => array(self::BELONGS_TO, 'Sessions', 'session_id'),
            'thread' => array(self::BELONGS_TO, 'ForumThreads', 'thread_id'),
			'owner_user' => array(self::BELONGS_TO, 'Users', 'owner_user_id'),
			'parent_post' => array(self::BELONGS_TO, 'ForumPosts', 'parent_post_id'),
			//'reply_posts' => array(self::HAS_MANY, 'ForumPosts', 'parent_post_id'),
			'old_post' => array(self::BELONGS_TO, 'ForumPosts', 'old_post_id'),
			'edited_posts' => array(self::HAS_ONE, 'ForumPosts', 'old_post_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'session_id' => 'Session',
			'timestamp' => 'Timestamp',
			'owner_user_id' => 'Автор',
			'parent_post_id' => 'Parent Post',
			'old_post_id' => 'Old Post',
			'message' => 'Текст сообщения',
		);
	}
	
	public function searchInThread($thread_id = null)
	{
		$criteria = new CDbCriteria;
		
		$criteria->with = 'owner_user';
		$criteria->compare('thread_id', $thread_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	
	public function searchInThreadHierarchical($thread_id = null)
	{
		$data_provider = $this->searchInThread($thread_id);
		$data_provider->pagination = false;
		
		$result = array();
		$positions = array();
		
		foreach ($data_provider->data as $value)
		{
			$parent_record = null;
			
			if ( empty($value->parent_post_id) )
			{
				$result[$value->id] = $value;
				$positions[$value->id] = &$result[$value->id];
			}
			else
			{
				$positions[$value->parent_post_id]->reply_posts[$value->id] = $value;
				$positions[$value->id] = &$positions[$value->parent_post_id]->reply_posts[$value->id];
			}
		}
		
		return $result;
	}
	
	public function create($data)
	{
		$this->setAttributes($data, false);
		
		$this->session_id = Yii::app()->user->session->id;
		$this->timestamp = time();
		
		if ( empty($this->owner_user_id) )
			$this->owner_user_id = Yii::app()->user->id;
		
		return $this->save();
	}
}
