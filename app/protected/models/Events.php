<?php

class Events extends BaseModel
{
	public $datetime_begin_date;
	public $datetime_begin_time;
	public $datetime_end_date;
	public $datetime_end_time;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'events';
	}

	public function rules()
	{
		return array(
			array('event_type, title, datetime_begin', 'required', 'message' => 'Поле «{attribute}» должно быть заполнено'),
			
			array('event_type', 'length', 'max' => 50),
			array('title', 'length', 'max' => 100),
			array('summary', 'length', 'max' => 255, 'tooLong' => 'Значение должно быть короче {max} символов'),
			array('gps_track', 'length', 'max' => 255),
			array('start_location', 'length', 'max' => 255),
			
			array('datetime_begin', 'date', 'format' => 'yyyy-MM-dd', 'message' => 'Неверный формат даты (YYYY-MM-DD)'),
			array('datetime_begin_date', 'date', 'message' => 'Неверный формат даты (YYYY-MM-DD)'),
			array('datetime_begin_time', 'type', 'type' => 'time', 'message' => 'Неверный формат времени (HH:MM)'),
			
			array('datetime_end', 'date', 'format' => 'yyyy-MM-dd', 'message' => 'Неверный формат даты (YYYY-MM-DD)'),
			array('datetime_end_date', 'date', 'message' => 'Неверный формат даты (YYYY-MM-DD)'),
			array('datetime_end_time', 'type', 'type' => 'time', 'message' => 'Неверный формат времени (HH:MM)'),
			
			array('event_type, title, summary, datetime_begin, datetime_begin_date, datetime_begin_time, datetime_end, datetime_end_date, datetime_end_time, gps_track, start_location', 'safe'),
		);
	}
	
	public function relations()
	{
		return array(
			'session' => array(self::BELONGS_TO, 'Sessions', 'session_id'),
			'thread' => array(self::BELONGS_TO, 'ForumThreads', 'thread_id'),
			'owner_user' => array(self::BELONGS_TO, 'Users', 'owner_user_id'),
			'old_event' => array(self::BELONGS_TO, 'Events', 'old_event_id'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'owner_user_id' => 'Организатор',
			'event_type' => 'Формат',
			'title' => 'Заголовок',
			'summary' => 'Краткое описание',
			'gps_track' => 'Маршрут',
			'picture' => 'Афиша',
			'start_location' => 'Место сбора',
			'datetime_begin' => 'Дата начала',
			'datetime_begin_date' => 'Дата начала',
			'datetime_begin_time' => 'Время',
			'datetime_end' => 'Дата завершения',
			'datetime_end_date' => 'Дата завершения',
			'datetime_end_time' => 'Время',
		);
	}
	
	public function searchInInterval($from, $to)
	{
		$criteria = new CDbCriteria;
		
		$criteria->addBetweenCondition('datetime_begin', $from, $to);
		$criteria->order = 'datetime_begin';
		$criteria->with = 'owner_user';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	
	public function searchInIntervalIndexByDate($from, $to)
	{
		$data_provider = $this->searchInInterval($from, $to);
		$data_provider->pagination = false;
		
		$result = array();
		
		foreach ($data_provider->data as $value)
		{
			$date = getdate(strtotime($value->datetime_begin));
			$result[$date['year']][$date['mon']][$date['mday']] = $value;
		}
		
		return $result;
	}
	
	public function create($data)
	{
		$this->setAttributes($data, false);
		
		$this->session_id = Yii::app()->user->session->id;
		$this->timestamp = time();
		
		$forum_thread = new ForumThreads;
		$thread_data = array(
			'forum_id' => ForumThreads::events_forum_id,
			'title' => $this->title,
			'summary' => $this->summary,
		);
		if ( !$forum_thread->create($thread_data) )
			return false;
		
		$this->thread_id = $forum_thread->id;
		
		if ( empty($this->owner_user_id) )
			$this->owner_user_id = Yii::app()->user->id;
		
		return $this->save();
	}
	
	protected function beforeValidate()
	{
		if ( !parent::beforeValidate() )
			return false;
		
		return true;
	}
	
	protected function beforeSave()
	{
		if ( !empty($this->datetime_begin) )
			$this->datetime_begin .= ' '.$this->datetime_begin_time;
		if ( !empty($this->datetime_end) )
			$this->datetime_begin .= ' '.$this->datetime_end_time;
		
		return true;
	}
	
	protected function afterFind()
	{
		$datetime_begin_stamp = strtotime( $this->datetime_begin );
		$this->datetime_begin_time = date('H:i', $datetime_begin_stamp);
		$this->datetime_begin_date = date('Y-m-d', $datetime_begin_stamp);
		
		$datetime_end_stamp = strtotime( $this->datetime_end );
		$this->datetime_end_time = date('H:i', $datetime_end_stamp);
		$this->datetime_end_date = date('Y-m-d', $datetime_end_stamp);
	}
}