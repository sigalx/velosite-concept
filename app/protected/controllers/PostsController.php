<?php

class PostsController extends BaseController
{
	public $pageTitle = 'Форум';
	
	public function init()
	{
		parent::init();
		
		$this->breadcrumbs = array(
			$this->pageTitle => array('index'),
		);
	}
	
	public function accessRules()
    {
        return array_merge(
			array(
				array('allow',
					'actions' => array('index')),
			),
			parent::accessRules()
		);
    }
	
	public function filters()
	{
        return array_merge(
			array(
				'postOnly + delete'
			),
			parent::filters()
		);
	}
	
	public function actionReply($to = null)
	{
		$parent_post = $this->loadModel($to);
		$model = new ForumPosts;

		$this->performAjaxValidation($model);

		if ( isset($_POST['ForumPosts']) )
		{
			$post_data = $_POST['ForumPosts'];
			$post_data['thread_id'] = $parent_post->thread_id;
			$post_data['parent_post_id'] = $parent_post->id;
			if ( $model->create($post_data) )
				$this->redirect(array('/threads/view', 'id' => $parent_post->thread_id ));
		}

		$this->render('reply',array(
			'model' => $model,
			'parent_post' => $parent_post,
		));
	}
	
	public function loadModel($id)
	{
		$model = ForumPosts::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
	
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'posts-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
