<?php

class EventsController extends BaseController
{
	public $pageTitle = 'Мероприятия';
	
	public function init()
	{
		parent::init();
		
		$this->breadcrumbs = array(
			$this->pageTitle => array('index'),
		);
	}
	
	public function accessRules()
    {
        return array_merge(
			array(
				array('allow',
					'actions' => array('index', 'year', 'month', 'day', 'view')),
			),
			parent::accessRules()
		);
    }
	
	public function filters()
	{
        return array_merge(
			array(
				'postOnly + delete'
			),
			parent::filters()
		);
	}
	
	public function actionIndex($id = null, $param0 = null, $param1 = null, $param2 = null)
	{
		$year = $id ? $id : $param0;
		$month = $param1;
		$day = $param2;
		
		/**/ if ($day)
			return $this->actionDay($year, $month, $day);
		else if ($month)
			return $this->actionMonth($year, $month);
		else if ($year)
			return $this->actionYear($year);
		
		$date = getdate();
		
		$this->redirect(array($this->id.'/'.$date['year'].'/'.sprintf('%02s', $date['mon'])));
		
		return $this->actionMonth($date['year'], $date['mon']);
	}
	
	public function actionYear($year = null)
	{
		$year = intval($year);
		
		$first_day_of_year= mktime(0, 0, 0, 1, 1, $year);
		$last_day_of_year = strtotime('last day of this month', mktime(0, 0, 0, 12, 1, $year));
		
		$this->render('year', array(
			'events' => Events::model()->searchInIntervalIndexByDate(
				unixtimeToDatetime($first_day_of_year),
				unixtimeToDatetime($last_day_of_year)
			),
			'year' => $year,
			'first_day_of_year' => $first_day_of_year,
			'last_day_of_year' => $last_day_of_year,
		));
	}
	
	public function actionMonth($year = null, $month = null)
	{
		$year = intval($year);
		$month = intval($month);
		
		if ( $month < 1 || $month > 12 )
			$this->throwHttpNotFound();
		
		$first_day_of_month = mktime(0, 0, 0, $month, 1, $year);
		$last_day_of_month = strtotime('last day of this month', $first_day_of_month);

		$begins_with = strtotime('last monday', $first_day_of_month);
		$ends_to = strtotime('next sunday', $last_day_of_month);
		
		$this->render('month', array(
			'events' => Events::model()->searchInIntervalIndexByDate(
				unixtimeToDatetime($begins_with),
				unixtimeToDatetime($ends_to)
			),
			'year' => $year,
			'month' => $month,
			'first_day_of_month' => $first_day_of_month,
			'last_day_of_month' => $last_day_of_month,
			'begins_with' => $begins_with,
			'ends_to' => $ends_to,
		));
	}
	
	public function actionDay($year = null, $month = null, $day = null)
	{
		$year = intval($year);
		$month = intval($month);
		$day = intval($day);
		
		if ( $month < 1 || $month > 12 || $day < 1 || $day > cal_days_in_month(CAL_GREGORIAN, $month, $year) )
			$this->throwHttpNotFound();

		$begins_with = mktime(0, 0, 0, $month, $day, $year);
		$ends_to = $begins_with + 24*60*60;
		
		$this->render('day', array(
			'events' => Events::model()->searchInInterval(
				unixtimeToDatetime($begins_with),
				unixtimeToDatetime($ends_to)
			),
			'year' => $year,
			'month' => $month,
			'day' => $day,
			'begins_with' => $begins_with,
			'ends_to' => $ends_to,
		));
	}
	
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$model = new Events;
		$post = new ForumPosts;

		$this->performAjaxValidation(array($model, $post));

		if (isset($_POST['Events']) && isset($_POST['ForumPosts']))
		{
			if ($model->create($_POST['Events']))
			{
				$post_data = $_POST['ForumPosts'];
				$post_data['thread_id'] = $model->thread_id;
				if ($post->create($post_data))
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array(
			'model' => $model,
			'post' => $post,
		));
	}

	public function loadModel($id)
	{
		$model = Events::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='events-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
