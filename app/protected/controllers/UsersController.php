<?php

class UsersController extends BaseController
{
	public $pageTitle = 'Пользователи';
	
	public function init()
	{
		parent::init();
		
		$this->breadcrumbs = array(
			$this->pageTitle => array('index'),
		);
	}
	
	public function accessRules()
    {
        return array_merge(
			array(
				array('allow',
					'actions' => array('index', 'login', 'register', 'vkauth')),
			),
			parent::accessRules()
		);
    }
	
	public function actionIndex()
	{
		$this->redirect('/users/login');
	}

	public function actionLogin()
	{
		if ( !Yii::app()->user->isGuest )
			$this->redirect(Yii::app()->homeUrl);
		
		$model = new LoginForm;
		
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		if (isset($_POST['LoginForm']))
		{
			$model->attributes = $_POST['LoginForm'];
			
			if ($model->validate() && $model->login())
				$this->redirect( Yii::app()->request->urlReferrer ? Yii::app()->request->urlReferrer : Yii::app()->returnUrl );
		}
		
		$this->render('login', array('model' => $model));
	}
	
	public function actionVkauth()
	{
		if ( !Yii::app()->user->isGuest )
			$this->redirect(Yii::app()->homeUrl);
		
		$user_identity = new VkAppIdentity();
		$user_identity->authenticate();
		
		if ( $user_identity->errorCode === BaseIdentity::ERROR_NONE &&
		     Yii::app()->user->login($user_identity) )
		{
			$this->redirect( Yii::app()->request->urlReferrer ? Yii::app()->request->urlReferrer : Yii::app()->returnUrl );
		}
		
		$this->redirect('/users/login');
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect( Yii::app()->request->urlReferrer ? Yii::app()->request->urlReferrer : Yii::app()->homeUrl );
	}
};
