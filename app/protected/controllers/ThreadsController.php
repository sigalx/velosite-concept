<?php

class ThreadsController extends BaseController
{
	public $pageTitle = 'Форум';
	
	public function init()
	{
		parent::init();
		
		$this->breadcrumbs = array(
			$this->pageTitle => array('index'),
		);
	}
	
	public function accessRules()
    {
        return array_merge(
			array(
				array('allow',
					'actions' => array('index', 'view')),
			),
			parent::accessRules()
		);
    }
	
	public function filters()
	{
        return array_merge(
			array(
				'postOnly + delete'
			),
			parent::filters()
		);
	}
	
	public function actionIndex()
	{
		$this->render('index', array(
			'threads' => ForumThreads::model()->searchForumThreads(),
		));
	}
	
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}
	
	public function actionCreate()
	{
		$model = new ForumThreads;
		$post = new ForumPosts;

		$this->performAjaxValidation(array($model, $post));

		if (isset($_POST['ForumThreads']) && isset($_POST['ForumPosts']))
		{
			$thread_data = $_POST['ForumThreads'];
			$thread_data['forum_id'] = ForumThreads::threads_forum_id;
			if ($model->create($thread_data))
			{
				$post_data = $_POST['ForumPosts'];
				$post_data['thread_id'] = $model->id;
				if ($post->create($post_data))
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array(
			'model' => $model,
			'post' => $post,
		));
	}
	
	public function loadModel($id)
	{
		$model = ForumThreads::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'threads-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
