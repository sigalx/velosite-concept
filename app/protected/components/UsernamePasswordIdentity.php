<?php

class UsernamePasswordIdentity extends BaseIdentity
{
	public function authenticate()
	{
		$this->errorCode = self::ERROR_PASSWORD_INVALID;
		
		$user = Users::model()->authenticateByPassword($this->username, $this->password);
		
		if ( !$user )
			return false;
		
		return $this->authorize($user);
	}
}