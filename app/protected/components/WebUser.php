<?php

class WebUser extends CWebUser
{
	public $id;
	public $session;
	
	protected $key_prefix;
	
	protected function changeIdentity($id, $name = '', $states = array())
	{
		parent::changeIdentity($id, $name, $states);
	}
	
	public function login($identity, $short_session = false)
	{	
		$this->clearStates();
		$states = $identity->getPersistentStates();
		$states['short_session'] = $short_session;
		
		if ( $this->beforeLogin($identity->getId(), $states, false) )
		{
			if ( !$this->allowAutoLogin && !$short_session )
				throw new CException( 'WebUser.allowAutoLogin must be set true in order to use cookie-based authentication.' );
			
			$this->changeIdentity($this->getId(), $this->getName());
			$this->saveToCookie($short_session);

			$this->afterLogin(false);
		}
		return !$this->getIsGuest();
	}

	protected function beforeLogin($id, $states, $from_cookie)
	{
		$this->setFlash('system-highlight', '');
		$this->setFlash('system-error', '');
		
		if ( !parent::beforeLogin($id, $states, $from_cookie) )
			return false;
			
		if ( !$from_cookie )
		{
			if ( !empty($states['disabled']) )
			{
				$this->setFlash('system-error', 'Этот аккаунт отключен');
				return false;
			}
			
			if ( !empty($states['locked_till']) && intval($states['locked_till']) > time() )
			{
				$this->setFlash('system-error', 'Этот аккаунт заблокирован до '.date('d F Y, H:i:s ', $states['locked_till']));
				return false;
			}
			
			if ( empty($states['auth_hash']) )
				die('Auth hash has not been provided.');
			
			if ( $this->session = Sessions::create( $id, $states['auth_hash'], $states['short_session'] ? time() + 10*60 : null ) ) // 10 min
			{
				$this->id = $id;
				$this->setState('auth_hash', $states['auth_hash']);
				return true;
			}
			
			die('Cannot write session.');
		}
		else if ( $this->session = Sessions::model()->findByAuthHash($this->getState('auth_hash')) )
		{
			if ( !empty($this->session->user->disabled) ||
			     !empty($this->session->user->locked_till) && intval($this->session->user->locked_till) > time() )
			{
				$this->logout();
				return false;
			}
			
			$this->id = $this->session->user_id;
			return !empty($this->session->user_id);
		}
		
		return false;
	}
	
	protected function beforeLogout()
	{
		$this->session = Sessions::model()->findByAuthHash($this->getState('auth_hash'));
		if ( !$this->session )
			return true;
		
		$this->session->close_time = time();
		$this->session->save();
		
		return true;
	}
	
	public function getStateKeyPrefix()
	{
		if ($this->key_prefix !== null)
			return $this->key_prefix;
		else
			return $this->key_prefix = md5( $_SERVER['REMOTE_ADDR'].Yii::app()->params['salt'] );
	}
	
	protected function saveToCookie($short_cookie = false)
	{
		$cookie = $this->createIdentityCookie( $this->getStateKeyPrefix() );
		if ($this->allowAutoLogin && !$short_cookie)
			$cookie->expire = time() + Yii::app()->params['cookie_ttl'];
		$cookie->value = $this->getState('auth_hash');
		Yii::app()->getRequest()->getCookies()->add($cookie->name, $cookie);
	}
	
	protected function restoreFromCookie()
	{
		$cookie = Yii::app()->getRequest()->getCookies()->itemAt($this->getStateKeyPrefix());
		
		if ( !$cookie || empty($cookie->value) || !is_string($cookie->value) )
			return;
		
		$this->setState('auth_hash', $cookie->value);

		if ( !$this->beforeLogin(0, array(), true) )
			return;
		
		$this->changeIdentity($this->getId(), $this->getName());
		if ($this->autoRenewCookie)
		{
			$cookie->expire = time() + Yii::app()->params['cookie_ttl'];
			Yii::app()->getRequest()->getCookies()->add($cookie->name, $cookie);
		}
		
		$this->afterLogin(true);
	}
	
	protected function renewCookie()
	{
		$cookie = Yii::app()->getRequest()->getCookies()->itemAt($this->getStateKeyPrefix());
		
		if ( !$cookie || empty($cookie->value) || !is_string($cookie->value) )
			return;
		
		$cookie->expire = time() + 1440;
		Yii::app()->getRequest()->getCookies()->add($cookie->name,$cookie);
	}
	
	public function loginRequired()
	{
		$app = Yii::app();
		$request = $app->getRequest();

		if ( !$request->getIsAjaxRequest() )
		{
			$this->setReturnUrl($request->getUrl());
			if (($url = $this->loginUrl) !== null)
			{
				if (is_array($url))
				{
					$route = isset($url[0]) ? $url[0] : $app->defaultController;
					$url = $app->createUrl( $route, array_splice($url, 1) );
				}
				
				//$request->redirect($url);
				if ( !$app->user->getFlash('system-error') )
					$app->user->setFlash('system-error', 'Для перехода на эту страницу необходимо авторизоваться');
				return $app->runController($url);
			}
		}
		elseif (isset($this->loginRequiredAjaxResponse))
		{
			echo $this->loginRequiredAjaxResponse;
			Yii::app()->end();
		}

		throw new CHttpException(403, Yii::t('yii','Login Required'));
	}
 
    public function getId()
    {
        return $this->id;
    }
 
    public function getName()
    {
        if ( !$this->getState('name') )
		{
			/**/ if ( !empty($this->session->user->name) )
				$this->setState('name', $this->session->user->name);
			else if ( !empty($this->session->user->login) )
				$this->setState('name', $this->session->user->login);
			else
				$this->setState('name', $this->id);
		}
		
		return $this->getState('name');
    }
	
	public function getIsGuest()
	{
		return !$this->getId();
	}
};
