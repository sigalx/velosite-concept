<?php

class BaseIdentity extends CUserIdentity
{
	protected $id;
	
	protected function authorize($user)
	{
		$this->id = $user->id;
		$this->setState('auth_hash', base64_encode( BaseModel::generateHash( $this->id.$this->username.uniqid(), $user->salt ) ));
		$this->setState('disabled', $user->disabled);
		$this->setState('locked_till', $user->locked_till);
		
		$this->errorCode = self::ERROR_NONE;
		return true;
	}
 
    public function getId()
    {
        return $this->id;
    }
}