<?php

class XAssetManager extends CAssetManager
{
	public function publish($path, $hash_by_name = false, $level = -1, $force_copy = null)
	{
		if ($path[0] != '/' )
			$path = './../app/assets/'.$path;
		
		return parent::publish($path, $hash_by_name, $level, $force_copy);
	}
};
