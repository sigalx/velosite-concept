<?php
/* @var $this EventsController */
/* @var $dataProvider CActiveDataProvider */

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish('styles/events/day.css') );
Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish('styles/threads/view.css') );

$this->pageTitle = $model->title;
$this->breadcrumbs = array_merge($this->breadcrumbs,
	array( $this->pageTitle => array('view', 'id' => $model->id) ));

echo XHtml::link( 'Создать мероприятие', array('create'), array('class' => 'big-link float-right') );

?>

<div class="mainbar">
<div class="event-calendar-day">
<div class="event">
	<?php echo XHtml::link( $model->title, array('view', 'id' => $model->id), array('class' => 'title') ); ?>
	<div class="summary">
		<?php echo XHtml::encode($model->summary); ?>
	</div>
	<div class="info">
		<?php echo $model->start_location.( $model->datetime_begin_time ? ( $model->start_location ? ', ' : '' ).$model->datetime_begin_time : '' ); ?>
	</div>
</div></div>

<?php $this->widget('ThreadPosts', array(
	'class' => 'thread-view',
	'posts' => $model->thread->searchPostsHierarchical(),
)); ?>

</div>

<?php echo $this->renderPartial( '_navigation_links' ); ?>
