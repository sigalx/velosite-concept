<?php
/* @var $this EventsController */
/* @var $dataProvider CActiveDataProvider */

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish('styles/events/year.css') );

$date = getdate();

$month_names_nominative = array
(
	'', // zero-index
	'Январь',
	'Февраль',
	'Март',
	'Апрель',
	'Май',
	'Июнь',
	'Июль',
	'Август',
	'Сентябрь',
	'Октябрь',
	'Ноябрь',
	'Декабрь',
);

if ($year === $date['year'])
	$this->pageTitle .= ' в этом году';
else
	$this->pageTitle .= ' '.$year;

$this->breadcrumbs = array(
	$this->pageTitle => array( '/'.$this->id.'/'.$year ),
);

?>

<?php echo XHtml::link( 'Создать мероприятие', array('create'), array('class' => 'big-link float-right') ); ?>

<div class="event-calendar-year mainbar">
<?php
for ( $month = 1; $month <= 12; $month++ )
{
	$first_day_of_month = mktime(0, 0, 0, $month, 1, $year);
	$last_day_of_month = strtotime('last day of this month', $first_day_of_month);

	$begins_with = strtotime('last monday', $first_day_of_month);
	$ends_to = strtotime('next sunday', $last_day_of_month);
	
	$this->widget('EventCalendarMonth', array(
		'class' => 'month-small',
		'title' => XHtml::link( $month_names_nominative[$month], array( 'events/'.$year.'/'.sprintf("%02s", $month) ) ),
		'events' => $events,
		'year' => $year,
		'month' => $month,
		'first_day_of_month' => $first_day_of_month,
		'last_day_of_month' => $last_day_of_month,
		'begins_with' => $begins_with,
		'ends_to' => $ends_to,
	));
}
?>
</div>

<?php echo $this->renderPartial( '_navigation_links' ); ?>
