<div class="navigation-links sidebar">
<?php
	$today = getdate();
	
	$next_year = getdate( strtotime( 'next year' ) );
	$next_month = getdate( strtotime( 'next month' ) );
	$next_day = getdate( strtotime( 'next day' ) );
	
	$in_curr_year_url = array( 'events/'.$today['year'] );
	$in_next_year_url = array( 'events/'.$next_year['year'] );

	$in_curr_month_url = array( 'events/'.$today['year'].'/'.sprintf('%02s', $today['mon']) );
	$in_next_month_url = array( 'events/'.$next_month['year'].'/'.sprintf('%02s', $next_month['mon']) );
	
	$in_curr_day_url = array( 'events/'.$today['year'].'/'.sprintf('%02s', $today['mon']).'/'.sprintf('%02s', $today['mday']) );
	$in_next_day_url = array( 'events/'.$next_day['year'].'/'.sprintf('%02s', $next_day['mon']).'/'.sprintf('%02s', $next_day['mday']) );
	
?>
	<ul>
<?php
		echo '<li>'.XHtml::link( 'Все в этом году', $in_curr_year_url ).'</li>';
		echo '<li>'.XHtml::link( 'В следующем году', $in_next_year_url ).'</li>';
?>
	</ul>
	<ul>
<?php
		echo '<li>'.XHtml::link( 'Все в этом месяце', $in_curr_month_url ).'</li>';
		echo '<li>'.XHtml::link( 'В следующем месяце', $in_next_month_url ).'</li>';
?>
	</ul>
	<ul>
<?php
		echo '<li>'.XHtml::link( 'Сегодня', $in_curr_day_url ).'</li>';
		echo '<li>'.XHtml::link( 'Завтра', $in_next_day_url ).'</li>';
?>
	</ul>
</div>
