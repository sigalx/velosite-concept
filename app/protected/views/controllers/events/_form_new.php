<?php $this->widget('XActiveForm', array(
	'id' => 'events-form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
	),
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
	'model' => $model,
	'fields' => array
	(
		'event_type' => array(
			'type' => 'dropdownlist',
			'items' => combineArray( array(
				'Совместная покатушка',
				'Туристическая поездка',
				'Бревет',
				'Соревнования',
			))
		),
		'title',
		'summary' => array( 'type' => 'textarea' ),
		'message' => array( 'type' => 'textarea', 'model' => $post ),
		'datetime_begin' => array( 'type' => 'datetime' ),
		'datetime_end' => array( 'type' => 'datetime' ),
		'start_location',
		'gps_track',
		'picture' => array( 'type' => 'file' ),
	),
)); ?>