<?php
/* @var $this EventsController */
/* @var $dataProvider CActiveDataProvider */

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish('styles/events/day.css') );

$date = getdate();

$month_names_genitive = array
(
	'', // zero-index
	'января',
	'февраля',
	'марта',
	'апреля',
	'мая',
	'июня',
	'июля',
	'августа',
	'сентября',
	'октября',
	'ноября',
	'декабря',
);

$this->pageTitle .= ' '.$day;
$this->pageTitle .= ' '.$month_names_genitive[$month];

if ($year !== $date['year'])
	$this->pageTitle .= ' '.$year;

$this->breadcrumbs = array(
	$this->pageTitle => array( '/'.$this->id.'/'.$year.'/'.sprintf("%02s", $month) ),
);

?>

<?php echo XHtml::link( 'Создать мероприятие', array('create'), array('class' => 'big-link float-right') ); ?>

<div class="event-calendar-day mainbar">
<?php echo empty($events->data) ? 'Ничего нет :(' : ''; ?>
<?php foreach ( $events->data as $event ): ?>
<div class="event">
	<?php echo XHtml::link( $event->title, array('view', 'id' => $event->id), array('class' => 'title') ); ?>
	<div class="summary">
		<?php echo XHtml::encode($event->summary); ?>
	</div>
	<div class="info">
		<?php echo $event->start_location.( $event->datetime_begin_time ? ( $event->start_location ? ', ' : '' ).$event->datetime_begin_time : '' ); ?>
	</div>
</div>
<?php endforeach ?>
</div>

<?php echo $this->renderPartial( '_navigation_links' ); ?>
