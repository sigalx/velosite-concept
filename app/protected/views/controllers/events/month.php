<?php
/* @var $this EventsController */
/* @var $dataProvider CActiveDataProvider */

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish('styles/events/month.css') );

$date = getdate();

$month_names_genitive = array
(
	'', // zero-index
	'января',
	'февраля',
	'марта',
	'апреля',
	'мая',
	'июня',
	'июля',
	'августа',
	'сентября',
	'октября',
	'ноября',
	'декабря',
);

$this->pageTitle .= ' '.$month_names_genitive[$month];

if ($year !== $date['year'])
	$this->pageTitle .= ' '.$year;

$this->breadcrumbs = array(
	$this->pageTitle => array( '/'.$this->id.'/'.$year.'/'.sprintf("%02s", $month) ),
);

?>

<?php

echo XHtml::link( 'Создать мероприятие', array('create'), array('class' => 'big-link float-right') );

$this->widget('EventCalendarMonth', array(
	'class' => 'event-calendar-month mainbar',
	'events' => $events,
	'year' => $year,
	'month' => $month,
	'first_day_of_month' => $first_day_of_month,
	'last_day_of_month' => $last_day_of_month,
	'begins_with' => $begins_with,
	'ends_to' => $ends_to,
));

echo $this->renderPartial( '_navigation_links' );

?>
