<?php
/* @var $this ForumController */
/* @var $model ForumPosts */

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish('styles/threads/view.css') );

$this->pageTitle = 'Ответить';
$this->breadcrumbs = array_merge($this->breadcrumbs,
	array( $this->pageTitle => array('reply', 'to' => $parent_post->id) ));
?>

<div class="thread-view">
<div class="forum-post">
	<div class="reference">
		от <span class="author"><?php echo $parent_post->owner_user->name ? $parent_post->owner_user->name : $parent_post->owner_user->login; ?></span>
	</div>
	<div class="timestamp"><?php echo date('d M Y, H:i', $parent_post->timestamp); ?></div>
	<div class="message"><?php echo XHtml::encode($parent_post->message); ?></div>
</div></div>

<?php $this->renderPartial('_form', array('model' => $model)); ?>
