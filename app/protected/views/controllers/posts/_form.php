<?php $this->widget('XActiveForm', array(
	'id' => 'posts-form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
	),
	'model' => $model,
	'fields' => array
	(
		'message' => array( 'type' => 'textarea' ),
	),
	'submit' => 'Ответить',
)); ?>
