<?php
/* @var $this EventsController */
/* @var $model Events */

$this->pageTitle = 'Создать обсуждение';
$this->breadcrumbs = array_merge($this->breadcrumbs,
	array( $this->pageTitle => array('create') ));

?>

<?php echo $this->renderPartial('_form', array('model' => $model, 'post' => $post)); ?>
