<?php
/* @var $this ForumController */
/* @var $dataProvider CActiveDataProvider */

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish('styles/threads/index.css') );

echo XHtml::link( 'Создать обсуждение', array('create'), array('class' => 'big-link float-right') );

?>

<div class="thread-index mainbar">
<?php foreach ($threads->data as $thread): ?>
<div class="forum-thread">
	<?php echo XHtml::link( $thread->title, array('view', 'id' => $thread->id), array('class' => 'title') ); ?>
</div>
<?php endforeach; ?>
</div>

<div class="sidebar">
</div>
