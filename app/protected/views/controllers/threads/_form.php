<?php $this->widget('XActiveForm', array(
	'id' => 'threads-form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
	),
	'model' => $model,
	'fields' => array
	(
		'title',
		'summary' => array( 'type' => 'textarea' ),
		'message' => array( 'type' => 'textarea', 'model' => $post ),
	),
)); ?>
