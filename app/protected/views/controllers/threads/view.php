<?php
/* @var $this ForumController */
/* @var $dataProvider CActiveDataProvider */

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish('styles/threads/view.css') );

$this->pageTitle = $model->title;
$this->breadcrumbs = array_merge($this->breadcrumbs,
	array( $this->pageTitle => array('view', 'id' => $model->id) ));

echo XHtml::link( 'Создать обсуждение', array('create'), array('class' => 'big-link float-right') );

?>

<?php $this->widget('ThreadPosts', array(
	'class' => 'thread-view mainbar',
	'posts' => $model->searchPostsHierarchical(),
)); ?>

<div class="sidebar">
</div>
