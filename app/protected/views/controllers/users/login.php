<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish('styles/users/login.css') );
$vk_logo = Yii::app()->assetManager->publish('images/vk-logo.png');
	
Yii::app()->clientScript->registerCss( 'vk-auth-button', <<<EOD
	.button.vkauth
	{
		background-image: url('$vk_logo');
	}
EOD
);

$this->pageTitle = 'Войти';
$this->breadcrumbs = array(
	$this->pageTitle => array( $this->action->id ),
);
?>

<?php $this->widget('XActiveForm', array(
	'id' => 'login-form',
	'action' => '/users/login',
	'enableClientValidation' => true,
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
	),
	'model' => $model,
	'fields' => array
	(
		'username',
		'password' => array( 'type' => 'password' ),
		//'short_session' => array( 'type' => 'checkbox' ),
	),
	'submit' => 'Войти',
	'buttons' => array
	(
		'vkauth' => XHtml::button( '', array('class' => 'button vkauth', 'onclick' => 'VK.Auth.login(getVkAuthLoginStatus)', 'title' => 'Войти через ВКонтакте') ),
		'submit' => XHtml::submitButton( 'Войти' ),
	)
)); ?>

