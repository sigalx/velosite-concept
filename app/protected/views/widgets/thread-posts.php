<?php foreach ($posts as $post): ?>

<div class="forum-post">
	<div class="reference">
		от <span class="author"><?php echo $post->owner_user->name ? $post->owner_user->name : $post->owner_user->login; ?></span>
		<span class="links">
			<?php echo XHtml::link( 'ответить', array('/posts/reply', 'to' => $post->id) ); ?>
		</span>
	</div>
	<div class="timestamp"><?php echo date('d M Y, H:i', $post->timestamp); ?></div>
	<div class="message"><?php echo XHtml::encode($post->message); ?></div>
	<div class="replies"><?php $this->render( 'thread-posts', array('posts' => $post->reply_posts) ); ?></div>
</div>

<?php endforeach ?>
