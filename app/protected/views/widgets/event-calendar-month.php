<div id="<?php echo $this->id; ?>" class="<?php echo $this->{'class'}; ?>">
<?php echo $title ? XHtml::tag('div', array('class' => 'title'), $title) : ''; ?>
<table>
<thead>
	<tr>
		<th>ПН</th>
		<th>ВТ</th>
		<th>СР</th>
		<th>ЧТ</th>
		<th>ПТ</th>
		<th class="weekend">СБ</th>
		<th class="weekend">ВС</th>
	</tr>
</thead>
<tbody>
<?php for ($t = $begins_with; $t < $ends_to; ) { ?>
	<tr>
	<?php
		for ($n = 1; $n <= 7; $n++, $t += 60*60*24)
		{
			$d = getdate($t);
			
			$html_classes = array();
			
			if ( isset($events[$d['year']][$d['mon']][$d['mday']]) )
				$html_classes[] = 'event-presented';
			
			/**/ if ( $t < $first_day_of_month || $t > $last_day_of_month )
				$html_classes[] = 'out-of-month';
			else if ( $n > 5 )
				$html_classes[] = 'weekend';
				
	?>	
			<td class="<?php echo implode(' ', $html_classes); ?>">
			<?php
				if ( isset($events[$d['year']][$d['mon']][$d['mday']]) )
					echo XHtml::link( XHtml::tag('span', array('class' => 'mday'), $d['mday']),
						array('events/'.$d['year'].'/'.sprintf("%02s", $d['mon']).'/'.sprintf("%02s", $d['mday'])) ); 
				else
					echo XHtml::tag('span', array('class' => 'mday'), $d['mday']);
			?>
			</td>
	<?php } ?>
	</tr>
<?php } ?>
</tbody></table></div>
