<?php

function startsWith($source, $head)
{
    return $head === "" || strpos($source, $head) === 0;
}

function endsWith($source, $tail)
{
    return $tail === "" || substr($source, -strlen($tail)) === $tail;
}

function unixtimeToDatetime($unixtime)
{
	return date('Y-m-d H:i:s', $unixtime);
}

function combineArray($keys, $values = null)
{
	if ( !$values )
		$values = $keys;
	return array_combine($keys, $values);
}
