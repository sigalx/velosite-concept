<?php

class EventCalendarMonth extends XWidget
{
	public $class = '';
	
	public $title = false;
	public $events = array();
	public $year = 1970;
	public $month = 1;
	public $first_day_of_month = 0;
	public $last_day_of_month = 0;
	public $begins_with = 0;
	public $ends_to = 0;
	
	public function init()
	{
	}

	public function run()
	{
		$this->render('event-calendar-month', array(
			'title' => $this->title,
			'events' => $this->events,
			'year' => $this->year,
			'month' => $this->month,
			'first_day_of_month' => $this->first_day_of_month,
			'last_day_of_month' => $this->last_day_of_month,
			'begins_with' => $this->begins_with,
			'ends_to' => $this->ends_to,
		));
	}
};
