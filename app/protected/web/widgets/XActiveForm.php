<?php

class XActiveForm extends _XActiveForm
{
	public $class = 'form';
	
	public $model;
	public $fields = array();
	public $submit;
	public $buttons = array();
	
	public function init()
	{
		XHtml::$before_required_input = XHtml::$beforeRequiredLabel;
		XHtml::$after_required_input = XHtml::$afterRequiredLabel;
		
		XHtml::$beforeRequiredLabel = '';
		XHtml::$afterRequiredLabel = '';
		
		$this->errorMessageCssClass = 'error-message';

		Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish('styles/forms.css') );
		
		echo '<div class="'.$this->class.'">';
		parent::init();
	}
	
	public function datetimeField($model, $attribute, $html_options = array())
	{
		return XHtml::activeDatetimeField($model, $attribute, $html_options);
	}
	
	public function run()
	{
		foreach ( $this->fields as $key => $value )
		{
			if (is_string($key))
			{
				$model = &$this->model;
				
				if ( isset( $value['model'] ) )
					$model = &$value['model'];
				
				$label_block = $this->labelEx($model, $key);
				$input_field = '';
				$hint_block = '';
				$error_block = $this->error($model, $key);
				
				if ( isset( $value['hint'] ) )
					$hint_block = XHtml::tag('div', array('class' => 'hint-message-back'), $value['hint'] );
				
				if ( isset( $value['type'] ) )
				{
					switch ( $value['type'] )
					{
					case 'text':
						$input_field = $this->textField($model, $key);
						break;
					case 'password':
						$input_field = $this->passwordField($model, $key);
						break;
					case 'textarea':
						$input_field = $this->textArea($model, $key);
						break;
					case 'checkbox':
						$label_block = '<div class="label"></div>';
						$input_field =
							$this->checkBox($model, $key).
							$this->labelEx($model, $key, array('no-wrapper' => 'no-wrapper'));
						break;
					case 'dropdown':
						$input_field = $this->dropDownList($model, $key, $value['items']);
						break;
					case 'datetime':
						
						$input_field = XHtml::tag( 'div', array('class' => 'datetime'),
							$this->textField($model, $key, array('class' => 'date')).
							$this->labelEx($model, $key.'_time').
							XHtml::tag( 'div', array('class' => 'display-inline-block'),
								XHtml::tag( 'span', array('class' => 'hint-message-back'), 'HH:MM' ).
								$this->textField($model, $key.'_time', array('class' => 'time'))
							)
						);
						
						$error_block = XHtml::tag( 'div', array('class' => 'error-message'),
							$this->error($model, $key).
							$this->error($model, $key.'_time')
						);
						
						break;
					case 'file':
						$input_field = $this->fileField($model, $key);
						break;
					}
				}
				else
					$input_field = $this->textField($model, $key);
				
				echo XHtml::tag( 'div', array('class' => 'row'),
					$label_block.$hint_block.$input_field.$error_block
				);
			}
			else
			{
				echo XHtml::tag( 'div', array('class' => 'row'),
					$this->labelEx($this->model, $value).
					$this->textField($this->model, $value).
					$this->error($this->model, $value) );
			}
		}
		
		$buttons_block = '';
		
		/**/ if ( $this->buttons )
		{
			foreach ( $this->buttons as $key => $value )
				$buttons_block .= $value;
		}
		else if ( $this->model )
		{
			$buttons_block = XHtml::submitButton( $this->submit ? $this->submit : ( $this->model->isNewRecord ? 'Создать' : 'Сохранить' ) );
		}
		
		echo XHtml::tag( 'div', array('class' => 'buttons'), $buttons_block );
		
		parent::run();
		echo '</div>';
	}
};
