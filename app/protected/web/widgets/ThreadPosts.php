<?php

class ThreadPosts extends XWidget
{
	public $class = '';
	
	public $posts = array();
	
	public function init()
	{
	}

	public function run()
	{
		$this->render('thread-posts-wrapper', array(
			'posts' => $this->posts,
		));
	}
};
