<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array
(
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'runtimePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'runtime',
	
	'name' => 'Veloclub-NN',

	'preload' => array('log'),

	'components' => array
	(
		'db' => array
		(
			'connectionString' => 'mysql:host=localhost;dbname=auth-test',
			'emulatePrepare' => true,
			'username' => 'auth-test',
			'password' => 'auth-test',
			'charset' => 'utf8',
		),
		
		'log' => array
		(
			'class' => 'CLogRouter',
			'routes' => array
			(
				array
				(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
);