<?php

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'bootstrap'.DIRECTORY_SEPARATOR.'CommonFunctions.php';

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array
(
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'runtimePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'runtime',
	
	'name' => 'Veloclub-NN',
	
	'sourceLanguage' => 'en_us',
	'language' => 'ru_ru',
	
	'preload' => array('log'),

	'import' => array
	(
		'application.models.*',
		'application.components.*',
		'application.web.helpers.*',
		'application.web.widgets.*',
	),

	'modules' => array
	(
		'gii' => array
		(
			'class' => 'system.gii.GiiModule',
			'password' => 'admin',
			'ipFilters' => array('95.79.50.115'),
		),
		
	),

	'components' => array
	(
		'assetManager' => array
		(
			'class' => 'XAssetManager',
			'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'assets',
		),
		
		'clientScript' => array
		(
			'scriptMap' => array
			(
				'jquery.js' => '//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.js',
				'jquery.min.js' => '//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js',
				
				'jquery-ui.js' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.js',
				'jquery-ui.min.js' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js',
				
				'jquery-ui.css' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css',
				'jquery-ui.min.css' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css',
			),
			'packages' => array
			(
				'jquery.ui' => array
				(
					'js' => array( YII_DEBUG ? 'jquery-ui.js' : 'jquery-ui.min.js' ),
					'css' => array( YII_DEBUG ? 'jquery-ui.css' : 'jquery-ui.min.css' ),
					'depends' => array( 'jquery' )
				)
			),
		),
		
		'db' => array
		(
			'connectionString' => 'mysql:host=localhost;dbname=auth-test',
			'emulatePrepare' => true,
			'username' => 'auth-test',
			'password' => 'auth-test',
			'charset' => 'utf8',
		),
		
		'urlManager' => array
		(
			'urlFormat' => 'path',
			'showScriptName' => false,
			'rules' => array
			(
				'<controller:\w+>/<id:\d+>' => '<controller>',
				
				'<controller:\w+>/<param0:\d+>/<param1:\d+>/<param2:\d+>/<param3:\d+>' => '<controller>',
				'<controller:\w+>/<param0:\d+>/<param1:\d+>/<param2:\d+>' => '<controller>',
				'<controller:\w+>/<param0:\d+>/<param1:\d+>' => '<controller>',
				'<controller:\w+>/<param0:\d+>' => '<controller>',
				
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				
				'<controller:\w+>/<action:\w+>/<param0:\w+>/<param1:\w+>/<param2:\w+>/<param3:\w+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<param0:\w+>/<param1:\w+>/<param2:\w+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<param0:\w+>/<param1:\w+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<param0:\w+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
		),
		
		'user' => array
		(
			'class' => 'WebUser',
			'allowAutoLogin' => true,
			'loginUrl' => '/users/login',
		),

		'session' => array
		(
            'class' => 'CDbHttpSession',
		),
		
		'errorHandler' => array
		(
			'errorAction' => '/site/error',
		),
		
		'log' => array
		(
			'class' => 'CLogRouter',
			'routes' => array
			(
				array
				(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
				
				// TODO
				array
				(
					'class' => 'CWebLogRoute',
					'categories' => 'system.db.CDbCommand',
				),
				
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => array
	(
		'salt' => 'gwk6QQtrF0XCUn5MkUQtIjzLHvAZYa8KVhIthBCR1YDrxEvpXhLSWG14w3VH4ujwwjDHasdTDMMnsFBDynP3EWqLrbyeMxO7yVHkuOilcLkF3ehSt2PXk76mrHdoQPjb',
		'cookie_ttl' => 86400,
		
		'vk_app_id' => 3023740,
		'vk_app_secret' => 'pmeOMjV59NEUrNyiVCfl',
	),
);