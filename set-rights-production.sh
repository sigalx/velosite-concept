#!/bin/bash

chown -R sigalx:www-data ./
chmod -R u=rwX,go=rX ./
chmod -R g+w assets/
chmod -R g+w runtime/
